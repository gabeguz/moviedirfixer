package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

type MovieResults struct {
	Page         int `json:"page"`
	TotalResults int `json:"total_results"`
	TotalPages   int `json:"total_pages"`
	Results      []struct {
		VoteCount        int     `json:"vote_count"`
		ID               int     `json:"id"`
		Video            bool    `json:"video"`
		VoteAverage      float64 `json:"vote_average"`
		Title            string  `json:"title"`
		Popularity       float64 `json:"popularity"`
		PosterPath       string  `json:"poster_path"`
		OriginalLanguage string  `json:"original_language"`
		OriginalTitle    string  `json:"original_title"`
		GenreIds         []int   `json:"genre_ids"`
		BackdropPath     string  `json:"backdrop_path"`
		Adult            bool    `json:"adult"`
		Overview         string  `json:"overview"`
		ReleaseDate      string  `json:"release_date"`
	} `json:"results"`
}

func main() {
	var apiKey = os.Getenv("THEMOVIEDB")
	var apiUrl = "https://api.themoviedb.org/3/search/movie/?api_key=" + apiKey + "&query="

	dir := "/shared/movies/"
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Println(err)
	}
	for _, file := range files {
		if file.IsDir() {
			query := strings.Replace(file.Name(), "_", " ", -1)
			query = apiUrl + url.QueryEscape(query)
			resp, err := http.Get(query)
			if err != nil {
				// handle error
				log.Println(err)
			}
			defer resp.Body.Close()

			// Fill the record with the data from the JSON
			var results MovieResults
			if err := json.NewDecoder(resp.Body).Decode(&results); err != nil {
				log.Println(err)
			}

			if len(results.Results) > 0 {
				fmt.Println("Page: ", results.Page)
				fmt.Println("Title: ", results.Results[0].Title)
				fmt.Println("Date:", results.Results[0].ReleaseDate)
				err := os.Rename("/shared/movies/"+file.Name(), "/shared/movies/"+results.Results[0].Title+" ("+strings.Split(results.Results[0].ReleaseDate, "-")[0]+")")
				if err != nil {
					log.Println(err)
				}
			}
		} else {
			log.Println("File: " + file.Name())
		}
		time.Sleep(1000 * time.Millisecond)
	}
}
